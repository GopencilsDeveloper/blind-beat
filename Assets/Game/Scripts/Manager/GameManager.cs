﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameState
{
    NULL, INTRO, MAINMENU, INGAME, WIN, LOSE,
}
public delegate void OnStateChangeHandler();
public class GameManager : MonoSingleton<GameManager>
{
    public GameState currentState;
    public event OnStateChangeHandler OnStateChange;

    public override void Initialize()
    {
        base.Initialize();
    }

    public void ChangeState(GameState state)
    {
        currentState = state;
        if (OnStateChange != null)
        {
            OnStateChange();
        }
    }
}
