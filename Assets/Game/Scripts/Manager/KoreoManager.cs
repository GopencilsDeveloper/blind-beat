﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using SonicBloom.Koreo.Players;

[System.Serializable]
public class Koreo
{
    public string eventID;
    public Koreography koreoGraphy;
}

public class KoreoManager : MonoSingleton<KoreoManager>
{
    #region SerializeField
    [Header("List KoreoGraphy")]
    [SerializeField] List<Koreo> koreoGraphyList;

    [Header("Serialize Field")]
    [SerializeField] private float hitThresholdInMs = 500;
    [SerializeField] private float perfectThresholdInMS = 200;
    #endregion

    #region Params
    private SimpleMusicPlayer smp;
    private AudioSource audioSource;
    private SpawnerController spawnerManager;
    private Koreography playingKoreo;
    private string eventID;
    private KoreographyTrackBase rhythmTrack;
    private List<KoreographyEvent> rawEvents;
    private Queue<KoreographyEvent> trackedNodes = new Queue<KoreographyEvent>();
    int accuracy = 0;
    int pendingIndex = 0;
    private int sampleRate;
    #endregion

    #region Properties
    public float SampleRate { get { return this.sampleRate; } }
    public AudioSource AudioSource { get { return this.audioSource; } }
    public string EventID { get { return this.eventID; } set { eventID = value; } }
    public int KoreoListCount { get { return this.koreoGraphyList.Count; } }
    #endregion

    #region Methods
    private void Start()
    {
        spawnerManager = SpawnerController.Instance;
        smp = GetComponent<SimpleMusicPlayer>();
        audioSource = GetComponent<AudioSource>();
        smp.Stop();
        audioSource.Stop();
        Init(1);
    }
    public void Init(int index)
    {
        playingKoreo = koreoGraphyList[index].koreoGraphy;
        eventID = koreoGraphyList[index].eventID;
        smp.LoadSong(playingKoreo, 0, false);

        rhythmTrack = playingKoreo.GetTrackByID(eventID);
        rawEvents = rhythmTrack.GetAllEvents();
        sampleRate = playingKoreo.SampleRate;
        Koreographer.Instance.RegisterForEvents(eventID, OnFireEvent);

        smp.Play();
        audioSource.Play();
    }

    void Update()
    {
        while (trackedNodes.Count > 0 && IsMissed())
        {
            trackedNodes.Dequeue();
        }

        SpawnNext();

        if (Input.GetKeyDown(KeyCode.D))
        {
            CheckNodeHit();
        }
    }

    void CheckNodeHit()
    {
        if (trackedNodes.Count > 0 && IsHit())
        {
            trackedNodes.Dequeue();
        }
    }
    public float deltaSec = 0f;
    void SpawnNext()
    {
        int currTimeInSample = playingKoreo.GetLatestSampleTime();

        while (pendingIndex < rawEvents.Count &&
        rawEvents[pendingIndex].StartSample <= currTimeInSample + (spawnerManager.timeObstacleSpawnOffset + spawnerManager.timeObstacleRushToPlayer) * playingKoreo.SampleRate)
        {
            trackedNodes.Enqueue(rawEvents[pendingIndex]);
            deltaSec = (float)(rawEvents[pendingIndex + 1].StartSample - rawEvents[pendingIndex].StartSample) / sampleRate;
            print("deltaSec: " + deltaSec);
            spawnerManager.SpawnObstacle();
            pendingIndex++;
        }

    }

    private bool IsMissed()
    {
        bool isMissed = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        isMissed = Mathf.Abs(currTimeInSample - nodeTime) > (0.001f * playingKoreo.SampleRate * hitThresholdInMs);

        return isMissed;
    }

    private bool IsHit()
    {
        bool isHit = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        float offset = Mathf.Abs(currTimeInSample - nodeTime);
        isHit = offset < (0.001f * playingKoreo.SampleRate * hitThresholdInMs);

        float offestInMs = Mathf.Abs(offset / playingKoreo.SampleRate) * 1000f;
        if (offestInMs <= perfectThresholdInMS)
        {
            accuracy = 0;
        }
        else
        if (offestInMs > perfectThresholdInMS && offestInMs <= hitThresholdInMs)
        {
            accuracy = 1;
        }
        return isHit;
    }

    private void OnFireEvent(KoreographyEvent evt)
    {
#if UNITY_EDITOR
        PlayerController.Instance.Turn *= -1;
        PlayerController.Instance.SpawnBullet();
#endif
    }
    #endregion
}
