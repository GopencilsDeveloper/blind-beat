﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;
using SonicBloom.Koreo;

public class SpawnerController : MonoSingleton<SpawnerController>
{
    #region SerializeField
    [SerializeField] private float safeDistance;
    public float obstacleSpeed;
    public float timeObstacleSpawnOffset;
    public float timeObstacleRushToPlayer;
    #endregion

    #region Params
    private PlayerController playerController;
    private KoreoManager koreoManager;
    private Vector2 obstacleSpawnPos = new Vector2();
    private Vector2 stuffSpawnPos = new Vector2();

    #endregion

    #region Methods

    public override void Initialize()
    {
        playerController = PlayerController.Instance;
        koreoManager = KoreoManager.Instance;
    }

    public float GetDistanceSpawnOffset()
    {
        float distanceObstacleRushToPlayer = timeObstacleRushToPlayer * obstacleSpeed;
        return playerController.MoveSpeed * timeObstacleSpawnOffset + safeDistance + distanceObstacleRushToPlayer;
    }

    public void SpawnObstacle()
    {
        var offset = Random.insideUnitCircle * 2f;
        if (playerController.Turn == 1)
        {
            obstacleSpawnPos = new Vector2(playerController.PlayerPos.x, playerController.PlayerPos.y + GetDistanceSpawnOffset());
            // stuffSpawnPos = new Vector2(playerController.PlayerPos.x - 1f, playerController.PlayerPos.y + GetDistanceSpawnOffset() + 1f);
            stuffSpawnPos = obstacleSpawnPos + offset;
        }
        else
        if (playerController.Turn == -1)
        {
            obstacleSpawnPos = new Vector2(playerController.PlayerPos.x + GetDistanceSpawnOffset(), playerController.PlayerPos.y);
            // stuffSpawnPos = new Vector2(playerController.PlayerPos.x + GetDistanceSpawnOffset() + 1f, playerController.PlayerPos.y - 1f);
            stuffSpawnPos = obstacleSpawnPos + offset;
        }
        ObjectPooler.Instance.SpawnFromPool("Obstacle", obstacleSpawnPos, Quaternion.identity);
        ObjectPooler.Instance.SpawnFromPool("Stuff", stuffSpawnPos, Quaternion.identity);
    }

    #endregion
}
