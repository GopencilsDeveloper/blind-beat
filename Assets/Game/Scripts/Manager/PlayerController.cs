﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DynamicLight2D;
using NaughtyAttributes;

public class PlayerController : MonoSingleton<PlayerController>
{
    #region SerializeField
    [SerializeField] private DynamicLight playerLight;
    [SerializeField] private TrailRenderer trail;
    [SerializeField] private GameObject bullet;
    [SerializeField] private SpriteRenderer sprite;
    [SerializeField] private float duration;

    [BoxGroup("Speed")]
    [SerializeField] private float moveSpeed;
    [BoxGroup("Speed")]
    [SerializeField] private float glowSpeed;

    [BoxGroup("Intensity")]
    [SerializeField] private float minIntensity;
    [BoxGroup("Intensity")]
    [SerializeField] private float maxIntensity;

    [BoxGroup("Color")]
    [SerializeField] private Color startColor;
    [BoxGroup("Color")]
    [SerializeField] private Color endColor;
    #endregion

    #region Params
    private float lastIntensity = 0;
    private float lastRadius = 0;
    private Vector2 playerPos;
    private int turn = 1;
    #endregion

    #region Properties
    public Vector2 PlayerPos { get { return this.playerPos; } set { this.playerPos = value; } }
    public float MoveSpeed { get { return this.moveSpeed; } set { this.moveSpeed = value; } }
    public int Turn { get { return this.turn; } set { this.turn = value; } }
    #endregion

    #region Methods
    public override void Initialize()
    {
        lastIntensity = playerLight.Intensity;
        lastRadius = playerLight.LightRadius;
        playerPos = transform.position;
    }

    private void Start()
    {
        GlowLight();
        LerpColorLight();
    }

    private void Update()
    {
        Move(moveSpeed, turn);

        if (Input.GetMouseButtonDown(0))
        {
            turn *= -1;
            SpawnBullet();
        }
        GlowSprite(KoreoManager.Instance.deltaSec);
    }
    float t = 0;
    float minAlpha = 0.2f;
    float maxAlpha = 1f;
    public void GlowSprite(float duration)
    {
        t += Time.deltaTime / duration;
        sprite.color = new Color(sprite.color.r, sprite.color.g, sprite.color.b, Mathf.Lerp(minAlpha, maxAlpha, t));
        if (t > 1f)
        {
            t = 0f;
        }
    }

    public void GlowLight()
    {
        StartCoroutine(C_Glow(minIntensity, maxIntensity, glowSpeed));
    }
    IEnumerator C_Glow(float min, float max, float speed)
    {
        float t = 0f;
        float lerpResult = 0;
        while (true)
        {
            t += Time.deltaTime;
            lerpResult = Mathf.Lerp(min, max, t);
            playerLight.Intensity = lerpResult;
            playerLight.LightRadius = lastRadius + lerpResult;
            yield return null;

            if (t > 1.0f)
            {
                SwapValue(ref min, ref max);
                t = 0.0f;
            }
            yield return null;
        }
    }

    private void SwapValue(ref float a, ref float b)
    {
        float temp = a;
        a = b;
        b = temp;
    }

    void Move(float moveSpeed, int turn = 1)
    {
        if (turn == 1)
        {
            playerPos.y += moveSpeed * Time.deltaTime;
        }
        else
        if (turn == -1)
        {
            playerPos.x += moveSpeed * Time.deltaTime;
        }
        transform.position = playerPos;
    }

    public void LerpColorLight()
    {
        StartCoroutine(C_ColorLerp());
    }
    IEnumerator C_ColorLerp()
    {
        float t = 0f;
        while (true)
        {
            t += Time.deltaTime;
            playerLight.LightColor = Color.Lerp(startColor, endColor, t);
            yield return null;

            if (t > 1.0f)
            {
                Color tempColor = startColor;
                startColor = endColor;
                endColor = tempColor;
                t = 0.0f;
            }
            yield return null;
        }
    }

    public void OnTriggerObstacle(Collider2D c)
    {
        print("Collider Obstacle");
    }

    public void SpawnBullet()
    {
        var bulletObj = ObjectPooler.Instance.SpawnFromPool("Bullet", transform.position, Quaternion.identity);
    }
    #endregion
}
