﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour, IPooledObject
{
    private int playerTurn;
    private float speed;
    private float timeToRush;
    private SpawnerController spawnerController;
    private SpriteRenderer spriteRenderer;
    [SerializeField] private Color startColor;
    [SerializeField] private Color endColor;

    public void Initialize()
    {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.playerTurn = PlayerController.Instance.Turn;
        this.spawnerController = SpawnerController.Instance;
        this.speed = SpawnerController.Instance.obstacleSpeed;
        this.timeToRush = SpawnerController.Instance.timeObstacleRushToPlayer;
    }

    public void OnObjectSpawn()
    {
        Initialize();
        ChangeColor();
        RushToPlayer();
        StartCoroutine(C_Disappear(3f));
    }

    public void ChangeColor()
    {
        startColor = Random.ColorHSV(250f / 360f, 330f / 360f, 1f, 1f, 1f, 1f, 1f, 1f);
        endColor = Random.ColorHSV(170f / 360f, 50f / 360f, 1f, 1f, 1f, 1f, 1f, 1f);
        spriteRenderer.material.SetColor("_Color", startColor);
        spriteRenderer.material.SetColor("_Color2", endColor);
    }

    public void RushToPlayer()
    {
        StartCoroutine(C_ObstacleRushToPlayer());
    }

    public IEnumerator C_ObstacleRushToPlayer()
    {
        Vector2 spawnPos = transform.position;
        float t = 0f;
        while (t < 1)
        {
            t += Time.deltaTime / timeToRush;
            if (playerTurn == 1)
            {
                spawnPos.y -= speed * Time.deltaTime;
            }
            else if (playerTurn == -1)
            {
                spawnPos.x -= speed * Time.deltaTime;
            }
            transform.position = spawnPos;
            yield return null;
        }
    }

    public IEnumerator C_Disappear(float delay)
    {
        yield return new WaitForSeconds(delay);
        gameObject.SetActive(false);
        var obj = ObjectPooler.Instance.SpawnFromPool("ParticleSystem", transform.position, Quaternion.identity);
        obj.GetComponent<ParticleSystem>().Play();
        yield return new WaitForEndOfFrame();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Bullet"))
        {
            StartCoroutine(C_Disappear(0f));
        }
    }
}
