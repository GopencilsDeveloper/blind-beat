﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour, IPooledObject
{
    public float speed;
    public float delayFire;
    private int playerTurn;
    private Vector2 position;

    public void Initialize()
    {
        this.playerTurn = PlayerController.Instance.Turn;
        position = transform.position;
    }

    public void OnObjectSpawn()
    {
        Initialize();
        StartCoroutine(C_Inactive());
    }

    private void Update()
    {
        if (playerTurn == 1)
        {
            position.x += speed * Time.deltaTime;
        }
        else
            if (playerTurn == -1)
        {
            position.y += speed * Time.deltaTime;
        }
        transform.position = position;
    }

    /*     public IEnumerator C_Fire()
        {
            yield return new WaitForSeconds(delayFire);
            while (true)
            {
                if (playerTurn == 1)
                {
                    position.x += speed * Time.deltaTime;
                }
                else
                if (playerTurn == -1)
                {
                    position.y += speed * Time.deltaTime;
                }
                transform.position = position;
                yield return null;
            }
        } */
    public IEnumerator C_Inactive()
    {
        yield return new WaitForSeconds(2f);
        gameObject.SetActive(false);
    }
}
