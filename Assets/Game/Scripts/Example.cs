﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using NaughtyAttributes;

public class Example : MonoBehaviour
{
    public float speed;
    public float duration;
    public float angleTarget = 270f;
    public int direction;

    private Vector3 rotation;
    private float angularVelocity;
    public void Initialize(int direction)
    {
        this.direction = direction;
        rotation = transform.eulerAngles;
        if (direction == 1)
        {
            rotation.z = angleTarget - GetOffsetRotationZ(duration, speed, angleTarget);
        }
        else
        if (direction == -1)
        {
            rotation.z = angleTarget + GetOffsetRotationZ(duration, speed, angleTarget);
        }
        transform.eulerAngles = rotation;
    }
    float GetOffsetRotationZ(float duration, float speed, float angleTarget)
    {
        float result = 0f;
        result = speed * duration;
        return result;
    }
    private void Update()
    {
        transform.Rotate(0f, 0f, direction * speed * Time.deltaTime);
    }
}
