﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameUltility
{
    public static void Swap(ref float a, ref float b)
    {
        float c = a;
        a = b;
        b = c;
    }
}
