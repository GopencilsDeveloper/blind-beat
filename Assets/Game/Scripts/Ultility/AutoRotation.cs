﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRotation : MonoBehaviour
{
    public bool clockWise = false;
    public float speed = 100f;
    private int direction = 1;
    private void Start()
    {
        direction = clockWise ? -1 : 1;
    }
    private void Update()
    {
        transform.Rotate(0f, 0f, direction * speed * Time.deltaTime);
    }

}
