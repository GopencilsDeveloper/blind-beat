﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StuffController : MonoBehaviour, IPooledObject
{
    public float speed;
    public Color startColor;
    public Color endColor;
    public float delayInactive;
    private int playerTurn;
    private SpriteRenderer spriteRenderer;
    private Vector2 pos;

    public void Initialize()
    {
        this.playerTurn = PlayerController.Instance.Turn;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        this.pos = transform.position;
        this.speed = Random.Range(1f, 4f);
    }
    public void OnObjectSpawn()
    {
        Initialize();
        ChangeColor();
        StartCoroutine(C_Inactive());
    }
    public void ChangeColor()
    {
        startColor = Random.ColorHSV(250f / 360f, 330f / 360f, 1f, 1f, 1f, 1f, 1f, 1f);
        endColor = Random.ColorHSV(170f / 360f, 50f / 360f, 1f, 1f, 1f, 1f, 1f, 1f);
        spriteRenderer.material.SetColor("_Color", startColor);
        spriteRenderer.material.SetColor("_Color2", endColor);
    }
    public IEnumerator C_Inactive()
    {
        yield return new WaitForSeconds(delayInactive);
        gameObject.SetActive(false);
    }
    private void Update()
    {
        if (playerTurn == 1)
        {
            pos.y -= speed * Time.deltaTime;
        }
        else if (playerTurn == -1)
        {
            pos.x -= speed * Time.deltaTime;
        }
        transform.position = pos;
    }
}
