﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SonicBloom.Koreo;
using DynamicLight2D;

public class TestScripts : MonoBehaviour
{
    private Koreography playingKoreo;
    private KoreographyTrackBase rhythmTrack;
    private List<KoreographyEvent> rawEvents;
    private Queue<KoreographyEvent> trackedNodes = new Queue<KoreographyEvent>();
    [SerializeField] private float hitThresholdInMs = 500;
    [SerializeField] private float perfectThresholdInMS = 200;

    [SerializeField] private DynamicLight2D.DynamicLight player;
    void Start()
    {
        playingKoreo = Koreographer.Instance.GetKoreographyAtIndex(0);
        rhythmTrack = playingKoreo.GetTrackByID("Piano");
        rawEvents = rhythmTrack.GetAllEvents();

    }

    void Update()
    {
        while (trackedNodes.Count > 0 && IsMissed())
        {
            trackedNodes.Dequeue();
        }

        SpawnNext();

        if (Input.GetKeyDown(KeyCode.D))
        {
            CheckNodeHit();
        }
    }

    void CheckNodeHit()
    {
        if (trackedNodes.Count > 0 && IsHit())
        {
            trackedNodes.Dequeue();
        }
    }

    int pendingIndex = 0;
    void SpawnNext()
    {
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        while (pendingIndex < rawEvents.Count && rawEvents[pendingIndex].StartSample < currTimeInSample)
        {
            trackedNodes.Enqueue(rawEvents[pendingIndex]);
            pendingIndex++;
        }
    }

    private bool IsMissed()
    {
        bool missed = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        missed = Mathf.Abs(currTimeInSample - nodeTime) > (0.001f * playingKoreo.SampleRate * 200f);

        return missed;
    }

    string accuracy = "";
    private bool IsHit()
    {
        bool isHit = false;
        int currTimeInSample = playingKoreo.GetLatestSampleTime();
        int nodeTime = trackedNodes.Peek().StartSample;
        float offset = Mathf.Abs(currTimeInSample - nodeTime) - (int)(0.001f * playingKoreo.SampleRate * hitThresholdInMs);
        isHit = offset <= 0 ? true : false;
        float offestInMs = Mathf.Abs(offset / playingKoreo.SampleRate) * 1000f;
        if (offestInMs > 0 && offestInMs <= perfectThresholdInMS)
        {
            accuracy = "Perfect";
        }
        else
        if (offestInMs > perfectThresholdInMS && offestInMs <= hitThresholdInMs)
        {
            accuracy = "Good";
        }

        print(accuracy);

        return isHit;
    }

}
